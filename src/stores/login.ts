import { defineStore } from "pinia";
import $axios, { addRestaurantIdInAPI } from "@/plugins/axios";
import type { TLogin, LoginForRestaurantResponse } from "@/type/TLogin";
import router from "@/router";

export const useLoginStore = defineStore("loginId", {
  state: () => ({
    login: [],
  }),
  actions: {
    async userLogin(form: TLogin) {
      try {
        localStorage.removeItem("restaurant_id");
        const response = await $axios.post("other/login", form);
        const data: LoginForRestaurantResponse = response.data;
        localStorage.setItem("restaurant_id", data.restaurant_id.toString());

        addRestaurantIdInAPI();

        await this.getAllStaff()
      } catch (error) {
        const e = error as Error;

        alert(e);
      }
    },
    // async getAllStaff(){
    //   const response = await $axios.post("auth/all", {
    //     restaurant_id: 1
    //   })
    // }
  },
});
