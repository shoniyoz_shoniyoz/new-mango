import axios from "axios";

const $axios = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_URL,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
  },
});
$axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.data.error) error.message = error.response.data.error;
    return Promise.reject(error);
  }
);

export default $axios;

export const addRestaurantIdInAPI: () => void = () => {
  $axios.interceptors.request.use(
    (request) => {
      const restaurant_id: number | null = Number(
        localStorage.getItem("restaurant_id")
      );
      if (restaurant_id) {
        if (!request.data) request.data = {};
        request.data.restaurant_id = restaurant_id;
      }

      return request;
    },
    (err) => {
      console.log("bu false");
      return Promise.reject(err);
    }
  );
};

export const addTokenInAPI: () => void = () => {
  const token: string | null = localStorage.getItem("token");

  if (token) {
    $axios.defaults.headers.common.authorization = `Bearer ${token}`;
  }
};

addRestaurantIdInAPI();
addTokenInAPI();
