import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";
import router from "./router";
import "./assets/main.css";

import axios from "./plugins/axios";
import VueAxios from "vue-axios";
const app = createApp(App);
app.use(VueAxios, axios);
app.use(createPinia());
app.use(router);

import {
  // create naive ui
  create,
  // component
  NButton,
  NLayout,
  NLayoutHeader,
  NLayoutContent,
  NSpace,
  NLayoutFooter,
  NLayoutSider,
  NGrid,
  NGi,
  NCard,
  NForm,
  NFormItem,
  NInput,
  NRow,
  NCol,


} from "naive-ui";

const naive = create({
  components: [
    NButton,
    NLayout,
    NLayoutHeader,
    NLayoutContent,
    NSpace,
    NLayoutFooter,
    NLayoutSider,
    NGrid,
    NGi,
    NCard,
    NForm,
    NFormItem,
    NInput,
    NRow,
    NCol,
  ],
});
app.use(naive);

app.mount("#app");
