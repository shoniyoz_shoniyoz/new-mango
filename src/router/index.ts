import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      meta: { title: "Login", layout: "default" },
      component: () => import('@/pages/PLogin/login.vue')
    }
  ]
})

export default router
